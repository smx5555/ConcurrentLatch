package org.zxp.ConcurrentLatch.demo;

import org.zxp.ConcurrentLatch.*;
import org.zxp.ConcurrentLatch.demo.dto.RuleDto;
import org.zxp.ConcurrentLatch.demo.service.PlatformLatch;
import org.zxp.ConcurrentLatch.demo.service.RuleLatch;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Test {
    public static void main(String[] args) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println(sdf.format(new Date()));

        ExecutorService excutor = Executors.newFixedThreadPool(Constants.CURRENT_MAX_POOL_COUNT);
        for (int i = 0; i < 300; i++) {
            excutor.execute(new Runnable() {
                public void run() {
                    try {
                        deal();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        Thread.sleep(70000);
        excutor.shutdownNow();
        LatchExcutorBlockingQueueManager.print();
        System.out.println(sdf.format(new Date()));
    }

    private static void deal() throws Exception {
        ConcurrentLatchExcutor excutor = new ConcurrentLatchExcutor();
        LatchThread platformLatchThread = new PlatformLatch();
        RuleDto ruleDto = new RuleDto();
        ruleDto.setRuleID("zxp123");
        ruleDto.setMmmm(0.00001);
        LatchThread ruleLatchThread = new RuleLatch(ruleDto);

        excutor.put(platformLatchThread);
        excutor.put(ruleLatchThread);
        Map<String, Object> map = excutor.excute();
        for (String key : map.keySet()) {
//            System.out.println("taskname==========" + key);
//            Object out = map.get(key);
//            System.out.println(out);
        }
    }
}
